#include <GyverTM1637.h>
#include <EEPROM.h>                       
#define CLK 10//pins definitions for the module and can be changed to other ports       
#define DIO 11
#define SET_BUTTON_PIN      A1
#define PLUS_BUTTON_PIN     A2
#define MINUS_BUTTON_PIN    A0
#define PUMP_BUTTON_PIN     A3
#define LED_PIN             13
#define PUMP_PIN             9
#define ROTATE_PIN          A7
#define BUZZER_PIN           5 // пин с пищалкой (англ. «buzzer»)
const    uint8_t  pinSensor = 2; 
         uint8_t  intSensor;  
volatile uint16_t varCount  = 0;
unsigned long timing; // Переменная для хранения точки отсчета
unsigned long timingSeg; // Переменная для хранения точки отсчета
boolean plusUp = true;
boolean minusUp = true;
boolean setUp = true;
boolean pumpUp = true;
int currentButton = 0;
int currentMode = 1;
int valueWatHigh = EEPROM.read(1);
int valueWatLow  = EEPROM.read(0);
float valueWatGlobal = 0;
int CountRpmPerLiter = EEPROM.read(3)+255;
boolean flagPumpTime = false;
boolean flagLedTime = false;
int PowerEngine = 0;
int x = 0;
int x1 = 0;
int changeNumberDec = 1;
GyverTM1637 disp(CLK, DIO);
void setup() {
  Serial.begin(9600);
  // put your setup code here, to run once:
  pinMode(SET_BUTTON_PIN , INPUT_PULLUP);
  pinMode(PLUS_BUTTON_PIN, INPUT_PULLUP);
  pinMode(MINUS_BUTTON_PIN, INPUT_PULLUP);
  pinMode(PUMP_BUTTON_PIN, INPUT_PULLUP);
  pinMode(ROTATE_PIN, INPUT);
  pinMode(LED_PIN,OUTPUT);
  pinMode(pinSensor, INPUT); 
  pinMode(PUMP_PIN, OUTPUT); 
  pinMode(BUZZER_PIN, OUTPUT);
  intSensor = digitalPinToInterrupt(pinSensor); 
  attachInterrupt(intSensor, funCountInt, RISING); 
  disp.brightness(7);//BRIGHT_TYPICAL = 2,BRIGHT_DARKEST = 0,BRIGHTEST = 7;
  disp.displayByte(_H, _E, _L, _O);   // вывести HELL, используя буквы из библиотеки
  if(digitalRead(SET_BUTTON_PIN) == LOW){Calibration();} 
  delay(1000);
  disp.clear();
  delay(200);
  disp.displayInt(CountRpmPerLiter);
  delay(1000);
  disp.clear();
  twistsH(EEPROM.read(1));                
  twistsL(EEPROM.read(0));
  disp.displayByte(3, _L);
  Serial.println(CountRpmPerLiter);
}

void loop() {

  
    Rotate();
  
  
  check ();
  switch (currentMode) {
       case 1:// основной цикл
        timing = millis();
        if(currentButton == 1){currentMode = 2;}

///////////////////////////////////////////////////

 if ( digitalRead(PUMP_BUTTON_PIN)== LOW){
       if (millis() - timingSeg > 600){ // Вместо 200подставьте нужное вам значение паузы
           x1++;
           if(x1==1){tone(BUZZER_PIN, 3000, 100);} 
           if(x1==2){tone(BUZZER_PIN, 3000, 100);} 
           if(x1==3){
            x1=0;tone(BUZZER_PIN, 3000, 300);
            valueWatGlobal = valueWatHigh+(float(valueWatLow)/10); 
            waterPump(valueWatGlobal);
            disp.clear();                  
            twistsH(EEPROM.read(1));                
            twistsL(EEPROM.read(0));
            disp.displayByte(3, _L);    // 3 ячейка, буква L
            }            
           timingSeg = millis();
          } 
  } else {x1=0;}
          
//////////////////////////////////////////////////
         
      currentButton = 0;
      break;
      
       case 2:
      //выполняется когда  var равно 2
      Serial.println("Режим конфигурации расхода воды");
      currentButton = 0;
      currentMode = 1;
      
      twistsH(valueWatHigh);
      twistsL(valueWatLow);
      disp.displayByte(3, _L);    // 3 ячейка, буква О
      while(currentButton != 4){
         if (millis() - timingSeg > 500){ // Вместо 200подставьте нужное вам значение паузы
           x++;
           if(x==1){disp.brightness(7);} 
           if(x==2){x=0;disp.brightness(0);} 
           timingSeg = millis();
          }
           
         check ();
         if(currentButton == 3 && changeNumberDec == 1){currentButton=0; valueWatHigh=constrain(valueWatHigh+1, 0, 50); twistsH(valueWatHigh); }
         if(currentButton == 2 && changeNumberDec == 1){currentButton=0; valueWatHigh=constrain(valueWatHigh-1, 0, 50); twistsH(valueWatHigh); }
         if(currentButton == 3 && changeNumberDec == 2){currentButton=0; valueWatLow=constrain(valueWatLow+1, 0, 9); twistsL(valueWatLow);}
         if(currentButton == 2 && changeNumberDec == 2){currentButton=0; valueWatLow=constrain(valueWatLow-1, 0, 9); twistsL(valueWatLow);}
        
         if(currentButton == 1){
          currentButton=0;
          changeNumberDec++;
           if(changeNumberDec==3){changeNumberDec=1;} 
          }
        
        }
      valueWatGlobal = valueWatHigh+(float(valueWatLow)/10);
      disp.brightness(7);
      tone(BUZZER_PIN, 3500, 400);
      disp.displayByte(_S,_A,_U,_E);
      EEPROM.write(1,valueWatHigh);                 
      EEPROM.write(0,valueWatLow);          
      Serial.println("Настройки сохранены!");
      Serial.print("Литров= ");
      Serial.println(valueWatGlobal);
      delay(1500);
      disp.clear();                  
      twistsH(EEPROM.read(1));                
      twistsL(EEPROM.read(0));
      disp.displayByte(3, _L);    // 3 ячейка, буква L
      currentButton = 0;
      currentMode = 1;
      break;

      case 3:
      //выполняется когда  var равно 3
      Serial.println("MI tut 3");
      currentButton = 0;
      break;
      
   
  }

  
}


void check(){
  // реагируем на нажатия с помощью функции, написанной нами
  plusUp  = handleClick(PLUS_BUTTON_PIN, plusUp,   3);
  minusUp = handleClick(MINUS_BUTTON_PIN, minusUp, 2);
  setUp   = handleClick(SET_BUTTON_PIN, setUp,     1);
  pumpUp  = handleClick(PUMP_BUTTON_PIN, pumpUp,   4);
}

boolean handleClick(int buttonPin, boolean wasUp, int delta)
{
  boolean isUp = digitalRead(buttonPin);
  if (wasUp && !isUp) {
    delay(10);
    isUp = digitalRead(buttonPin);
    // если был клик, меняем яркость в пределах от 0 до 255
    if (!isUp)    
      currentButton = delta;
      Serial.println(delta);
      tone(BUZZER_PIN, 3000, 30);
  }
  return isUp; // возвращаем значение обратно, в вызывающий код
}

void funCountInt(){varCount++;Serial.println(varCount);}

float waterPump(float val){
  val=val*CountRpmPerLiter;
  Serial.print("Кол во импульсов для n дитров= ");
  Serial.println(val);
  varCount=0;
  Serial.print("Наливаю! ");
  disp.clear();
  disp.displayByte(0x3f,0x54,0x00,0x00);
  while(varCount<val){
    Rotate();
    analogWrite(PUMP_PIN,PowerEngine);
  }  
    Serial.print("Готово! ");
    disp.clear();
    disp.displayByte(0x3f,0x71,0x71,0x00);
    varCount=0;
    analogWrite(PUMP_PIN,0);
    tone(BUZZER_PIN, 3000, 1500);
    delay(1000);
    disp.clear();
}

int twistsH(int H){
   disp.point(true);
  if (H>9){
      disp.twist(0, H/10, 10);
      disp.twist(1, H%10, 10); 
     }else {
          //disp.twist(0, 0, 10);
          disp.displayByte(0, 0x00);  
          disp.twist(1, H%10, 10);
            }         
}

int twistsL(int L){
   disp.twist(2, L, 20);  
  }

void Calibration(){
int tmpval=0;
Serial.println("Калибровка счетчика воды!");
Serial.println("Нажмите и удерживайте кнопку PUSH пока не нальётся 1 литр жидкости!");
disp.displayByte(_C,_O,_N,_F);
delay(1000);
disp.displayByte(_P,_U,_S,_H);
varCount = 0;
while (digitalRead(PUMP_BUTTON_PIN) != LOW){};
disp.clear();
while (digitalRead(PUMP_BUTTON_PIN) != HIGH){
       Rotate();
       analogWrite(PUMP_PIN,PowerEngine);
       disp.displayInt(varCount);
       }
analogWrite(PUMP_PIN,0);
CountRpmPerLiter = varCount;
varCount = 0;
if(CountRpmPerLiter > 255 && CountRpmPerLiter <= 510 ){
    Serial.print("rpm= ");
  Serial.println(CountRpmPerLiter);
  EEPROM.write(3,CountRpmPerLiter-255);
  CountRpmPerLiter = EEPROM.read(3)+255;
  Serial.println("Калибровка закончена!");
  disp.displayByte(_S,_A,_U,_E);
  tone(BUZZER_PIN, 3000, 500);
  delay(1000);
  disp.clear();
  Serial.println(CountRpmPerLiter);
  } 
    else{
      Serial.println("Ошибка калибровки счетчика воды!");
      tone(BUZZER_PIN, 3400, 1500);
      disp.displayByte(_E,_r,_o,_r);
      delay(1000);
       EEPROM.write(3,0);
       CountRpmPerLiter = EEPROM.read(3); 
      }
  
 
}

void Rotate(){
  //int y;
  int x =analogRead(ROTATE_PIN);
  PowerEngine = map(x, 0, 1020, 0, 255);
  //y= map(x, 0, 1000, 0, 10);
  //Serial.println(PowerEngine);
  //delay(300);
  //return x;
} 
